﻿namespace TwitchChatNotifier
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using Newtonsoft.Json;

    public class Program
    {
        static Program()
        {
            BlackListViewers = new List<string>
            {
                "streamlabs",
                "skinnyseahorse",
                "P0sitivitybot",
                "bananennanen",
                "commanderrott",
                "laf21",
                "n0tahacker_", 
                "sickfold", 
                "slocool",
                "teyd__",
                "thefirstdemon"
            };
        }

        public static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Enter username as an argument.");
                return;
            }

            if (args.Length > 0)
            {
                if (args.Length == 2)
                {
                    Username = args[0];
                    Frequency = int.Parse(args[1]);
                }
                else if (args.Length == 1)
                {
                    Username = args[0];
                }
                
                Url = $"https://tmi.twitch.tv/group/user/{Username}/chatters";
            }
            
            var timer = new Timer(CheckViewers, null, 0, Frequency * 1000);
            
            Console.ReadLine();
        }

        private static void CheckViewers(Object o) 
        {
            Console.WriteLine($"{DateTime.Now}: Scanning {Username}!");
            var client = new HttpClient();
            var chatJson = client.GetStringAsync(Url).Result;

            var chat = JsonConvert.DeserializeObject<Chat>(chatJson);
            
            if (PreviousChat == null)
            {
                PreviousChat = chat;
                return;
            }
            
            var newViewer = false;
            foreach (var viewer in chat.Chatters.Viewers)
            {
                if (BlackListViewers.Contains(viewer, StringComparer.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                if (!PreviousChat.Chatters.Viewers
                    .Contains(viewer, StringComparer.InvariantCultureIgnoreCase))
                {            
                    Console.WriteLine($"{DateTime.Now}: Welcome, {viewer}!");
                    newViewer = true;
                }                
            }

            if (newViewer)
            {                
                Console.Beep();
                newViewer = false;
            }

            PreviousChat = chat;
        }

        private static string Username { get; set; }
        private static int Frequency { get; set; } = 10;
        private static string Url { get; set; }
        private static Chat PreviousChat { get; set; }
        private static List<string> BlackListViewers { get; set; }
    }
}
