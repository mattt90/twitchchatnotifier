namespace TwitchChatNotifier
{
    using Newtonsoft.Json;

    public class Chat
    {
        [JsonProperty("_links")]
        public object Links { get; set; }
        [JsonProperty("chatter_count")]
        public int ChatterCount { get; set; }
        [JsonProperty("chatters")]
        public Chatters Chatters { get; set; }
    }
}
