namespace TwitchChatNotifier
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Chatters
    {        
        [JsonProperty("vips")]
        public IEnumerable<string> Vips { get; set; }        
        [JsonProperty("moderators")]
        public IEnumerable<string> Moderators { get; set; }
        [JsonProperty("staff")]
        public IEnumerable<string> Staff { get; set; }
        [JsonProperty("admins")]
        public IEnumerable<string> Admins { get; set; }
        [JsonProperty("global_mods")]
        public IEnumerable<string> GlobalMods { get; set; }
        [JsonProperty("viewers")]
        public IEnumerable<string> Viewers { get; set; }
    }
}
